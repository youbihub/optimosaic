cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

# ---- Project ----

# Note: update this to your new project's name and version
project(
  Optimos
  VERSION 1.0
  LANGUAGES CXX
)

# ---- Include guards ----

if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
  message(
    FATAL_ERROR
      "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there."
  )
endif()

# ---- Add dependencies via CPM ----
# see https://github.com/TheLartians/CPM.cmake for more info

include(cmake/CPM.cmake)

# PackageProject.cmake will be used to make our target installable
CPMAddPackage(
  NAME PackageProject.cmake
  GITHUB_REPOSITORY TheLartians/PackageProject.cmake
  VERSION 1.3
)
# Eigen
CPMAddPackage(
  NAME Eigen
  GIT_TAG master
  GITLAB_REPOSITORY libeigen/eigen
  DOWNLOAD_ONLY YES
)
if(Eigen_ADDED)
  add_library(Eigen INTERFACE IMPORTED)
  target_include_directories(Eigen INTERFACE ${Eigen_SOURCE_DIR})
endif()

# Ceres
find_package(Ceres REQUIRED)
# CPMFindPackage(
#   NAME ceres
#   GITHUB_REPOSITORY ceres-solver/ceres-solver
#   GIT_TAG 2.0.0
#   OPTIONS # "CMAKE_BUILD_TYPE Release" #not needed
#           "BUILD_EXAMPLES OFF" "BUILD_TESTING OFF" "EXPORT_BUILD_DIR ON"
# )
# OpenCV CPMAddPackage( NAME OpenCV GITHUB_REPOSITORY opencv/opencv GIT_TAG master OPTIONS
# "BUILD_PROTOBUF OFF" "BUILD_PERF_TESTS OFF" "BUILD_TESTS OFF" "BUILD_opencv_python3 OFF"
# "BUILD_opencv_flann OFF" "BUILD_opencv_ml OFF" "BUILD_opencv_photo OFF" "BUILD_opencv_dnn OFF"
# "BUILD_opencv_features2d OFF" "BUILD_opencv_gapi OFF" "BUILD_opencv_ts OFF" "BUILD_opencv_video
# OFF" )
find_package(OpenCV REQUIRED)
message(${OpenCV_LIBS})
# ---- Add source files ----

# Note: globbing sources is considered bad practice as CMake's generators may not detect new files
# automatically. Keep that in mind when changing files, or explicitly mention them here.
file(GLOB_RECURSE headers CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
file(GLOB_RECURSE sources CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/source/*.cpp")

# ---- Create library ----

# Note: for header-only libraries change all PUBLIC flags to INTERFACE and create an interface
# target: add_library(Optimos INTERFACE) set_target_properties(Optimos PROPERTIES
# INTERFACE_COMPILE_FEATURES cxx_std_17)

add_library(Optimos ${headers} ${sources})

set_target_properties(Optimos PROPERTIES CXX_STANDARD 20)
target_compile_options(Optimos PUBLIC -ftemplate-depth=4096)

# being a cross-platform target, we enforce standards conformance on MSVC
target_compile_options(Optimos PUBLIC "$<$<BOOL:${MSVC}>:/permissive->")

target_link_libraries(Optimos PUBLIC Eigen ${OpenCV_LIBS} Ceres::ceres)

target_include_directories(
  Optimos PUBLIC $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
                 $<INSTALL_INTERFACE:include/${PROJECT_NAME}-${PROJECT_VERSION}>
)

# ---- Create an installable target ----
# this allows users to install and find the library via `find_package()`.

# the location where the project's version header will be placed should match the project's regular
# header paths
string(TOLOWER ${PROJECT_NAME}/version.h VERSION_HEADER_LOCATION)

packageProject(
  NAME ${PROJECT_NAME}
  VERSION ${PROJECT_VERSION}
  BINARY_DIR ${PROJECT_BINARY_DIR}
  INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include
  INCLUDE_DESTINATION include/${PROJECT_NAME}-${PROJECT_VERSION}
  VERSION_HEADER "${VERSION_HEADER_LOCATION}"
  DEPENDENCIES ""
)
