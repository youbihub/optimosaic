#pragma once

#include <Eigen/Core>
#include <Eigen/Dense>
#include <iostream>
#include <map>
#include <opencv2/core/eigen.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "ceres/ceres.h"
#include "ceres/cubic_interpolation.h"

namespace optimos {

  struct Topo {
    Eigen::Matrix2Xd xy;        // col maj
    Eigen::MatrixXi tiles;      // row maj
    Eigen::Matrix2Xi constidx;  // 0=x,1=y,2=xy
  };

  auto MakeTopoRect(const size_t r, const size_t c, const int y_res, const int x_res) {
    auto xy{Eigen::Matrix2Xd(2, (r + 1) * (c + 1))};
    {  // todo: std::invoke+ lambda
      auto row{Eigen::VectorXd::LinSpaced(c + 1, 0, x_res).transpose()};
      auto col{Eigen::VectorXd::LinSpaced(r + 1, 0, y_res)};

      auto k(0);
      for (size_t i = 0; i < c + 1; i++) {
        for (size_t j = 0; j < r + 1; j++) {
          xy.col(k++) << row(i), col(j);
        }
      }
    }
    // row major for tiles for easier integration with opencv for init
    auto tiles{Eigen::MatrixXi(4, r * c)};
    {
      int k = 0;
      for (size_t i = 0; i < r; i++) {
        for (size_t j = 0; j < c; j++) {
          auto idx0{i + j * (r + 1)};
          tiles.col(k++) << idx0, idx0 + r + 1, idx0 + r + 2, idx0 + 1;
        }
      }
    }
    auto constidx{Eigen::Matrix2Xi{2, 2 * (c + r)}};
    {
      auto k{0};
      // top edge
      {
        constidx.col(k++) << 0 * (r + 1), 2;
        for (size_t i = 1; i < c + 1 - 1; i++) {
          constidx.col(k++) << i * (r + 1), 1;
        }
      }
      // right edge
      {
        auto i0{c * (r + 1)};
        constidx.col(k++) << i0 + 0, 2;
        for (size_t i = 1; i < r + 1 - 1; i++) {
          constidx.col(k++) << i0 + i, 0;
        }
      }
      // bottom edge
      {
        auto i0{r + c * (r + 1)};
        constidx.col(k++) << i0 - 0 * (r + 1), 2;
        for (size_t i = 1; i < c + 1 - 1; i++) {
          constidx.col(k++) << i0 - i * (r + 1), 1;
        }
      }
      // left edge
      {
        auto i0{r};
        constidx.col(k++) << i0 - 0, 2;
        for (size_t i = 1; i < r + 1 - 1; i++) {
          constidx.col(k++) << i0 - i, 0;
        }
      }
    }
    return Topo{xy, tiles, constidx};
  }

  // Poly needs to be oriented (otherwise area negative)
  template <typename T>
  auto PolyArea(const Eigen::MatrixBase<T>& p0, const Eigen::MatrixBase<T>& p1,
                const Eigen::MatrixBase<T>& p2, const Eigen::MatrixBase<T>& p3) {
    // make this elegant and generic to number of points pls
    auto area{typename T::Scalar{0.}};

    area += p0(0) * p1(1) - p0(1) * p1(0);
    area += p1(0) * p2(1) - p1(1) * p2(0);
    area += p2(0) * p3(1) - p2(1) * p3(0);
    area += p3(0) * p0(1) - p3(1) * p0(0);

    return area / 2.;
  }

  // quadri: p0..p3. u: p0->p1 or p3->p2, v p0->p3 or p1->p2
  template <typename T>
  auto PointInQuadri(const Eigen::MatrixBase<T>& p0, const Eigen::MatrixBase<T>& p1,
                     const Eigen::MatrixBase<T>& p2, const Eigen::MatrixBase<T>& p3,
                     const double& u, const double& v) {
    auto r0 = p0 + u * (p1 - p0);
    auto r1 = p3 + u * (p2 - p3);

    return r0 + v * (r1 - r0);
  }

  struct ColorResidual {
    template <typename M1>
    ColorResidual(const cv::Mat& im, const Eigen::MatrixBase<M1>& vu, const double dd)
        : image(im), uv(vu), du(dd) {}

    template <typename T> bool operator()(const T* const p0, const T* const p1, const T* const p2,
                                          const T* const p3, const T* const co, T* residual) const {
      auto p0_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>(p0)};
      auto p1_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>(p1)};
      auto p2_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>(p2)};
      auto p3_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>(p3)};
      auto co_m{Eigen::Map<const Eigen::Matrix<T, 3, 1>>(co)};

      auto& u = uv(0);
      auto& v = uv(1);

      auto q0{PointInQuadri(p0_m, p1_m, p2_m, p3_m, u, v)};
      auto q1{PointInQuadri(p0_m, p1_m, p2_m, p3_m, u + du, v)};
      auto q2{PointInQuadri(p0_m, p1_m, p2_m, p3_m, u + du, v + du)};
      auto q3{PointInQuadri(p0_m, p1_m, p2_m, p3_m, u, v + du)};

      auto pm{PointInQuadri(q0, q1, q2, q3, 0.5, 0.5)};  // todo: not necessary?
      auto color_pix_interp{
          Eigen::Matrix<T, 3, 1>{}};  // color could be weighted sum of interpolated neigbors
                                      // corners neighbours +center, like (c0+c1+c2+c3+4*cc)/8

      ceres::Grid2D<uchar, 3> array(image.data, 0, image.rows, 0, image.cols);
      ceres::BiCubicInterpolator interpolator(array);  // todo: as member.?
      interpolator.Evaluate(pm(1), pm(0), color_pix_interp.data());

      auto area{PolyArea(q0, q1, q2, q3)};  // todo: packing/unpacking [...]
      if (area < 0.) {
        return false;
      }

      auto res_m{Eigen::Map<Eigen::Matrix<T, 3, 1>>{residual}};
      res_m = pow(area, 0.5) * (color_pix_interp - co_m);
      return true;
    }

    const cv::Mat& image;
    Eigen::Vector2d uv;  // smallest couple for subpixel
    double du;           // increment for u and v
  };

  auto ColorInit(const cv::Mat& frame, const int ntilerow, const int ntilecol) {
    auto resized{cv::Mat()};
    cv::resize(frame, resized, {ntilecol, ntilerow}, 0, 0, cv::INTER_AREA);
    auto colorx{Eigen::Matrix3Xd::Zero(3, ntilerow * ntilecol).eval()};
    auto kk{0};  // todo: remove. make elegant, functino already there
    for (int i = 0; i < ntilerow; i++) {
      for (int j = 0; j < ntilecol; j++) {
        colorx.col(kk)(0) = (double)resized.at<cv::Vec3b>(i, j).val[0];  // todo: /255?
        colorx.col(kk)(1) = (double)resized.at<cv::Vec3b>(i, j).val[1];
        colorx.col(kk)(2) = (double)resized.at<cv::Vec3b>(i, j).val[2];
        kk++;
      }
    }

    return colorx;
  }

  // struct ConvexResidual {
  //   template <typename T>
  //   bool operator()(const T* const p1, const T* const p2, const T* const p3, T* res) const {
  //     auto p1_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{p1}};
  //     auto p2_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{p2}};
  //     auto p3_m{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{p3}};

  //     auto is_convex{IsInside(p3_m, p1_m, p2_m)};
  //     res[0] = (T)0;
  //     return is_convex;
  //   }
  // };
  struct MosaicProblem {
    MosaicProblem(const cv::Mat& frame, const size_t r, const size_t c)
        : problem(ceres::Problem{}),
          options(ceres::Solver::Options{}),
          summary(ceres::Solver::Summary{}),
          topo(MakeTopoRect(r, c, frame.rows, frame.cols)),
          colorx(ColorInit(frame, r, c)) {
      // 500 represents approx the horizontal resolution of interest
      auto nx{size_t(500. / c)};
      // auto ny{size_t(500. * frame.rows / frame.cols / r)};//todo:
      std::cout << "is continuous:" << frame.isContinuous() << std::endl;
      // cv::resize(frame, frameb, {(int)c * 4, (int)r * 4}, 0, 0,
      // cv::InterpolationFlags::INTER_AREA);
      cv::GaussianBlur(frame, frame, {}, (double)frame.cols / c / nx / 3.);
      cv::imwrite("blurred.png", frame);
      auto uv{Eigen::Vector2d{}};
      for (long int i = 0; i < topo.tiles.cols(); i++) {
        auto p0 = topo.xy.col(topo.tiles(0, i));
        auto p1 = topo.xy.col(topo.tiles(1, i));
        auto p2 = topo.xy.col(topo.tiles(2, i));
        auto p3 = topo.xy.col(topo.tiles(3, i));
        auto colo = colorx.col(i);
        for (size_t iu = 0; iu < nx; iu++) {
          uv(0) = (double)iu / nx;
          for (size_t iv = 0; iv < nx; iv++) {
            uv(1) = (double)iv / nx;
            problem.AddResidualBlock(
                new ceres::AutoDiffCostFunction<ColorResidual, 3, 2, 2, 2, 2, 3>(
                    new ColorResidual(frame, uv, 1. / nx)),
                nullptr, p0.data(), p1.data(), p2.data(), p3.data(), colo.data());
          }
        }
      }

      // lock edges
      for (long int i = 0; i < topo.constidx.cols(); i++) {
        // last subsetparameterizartion erase previous, so cant lock corners effectively
        auto ixy{topo.constidx(0, i)};
        // auto iel{topo.constidx(1, i)};
        auto constparam{std::vector<int>{}};
        problem.SetParameterBlockConstant(topo.xy.col(ixy).data());
        // switch (iel) {
        //   case 0:
        //     problem.SetParameterization(topo.xy.col(ixy).data(),
        //                                 new ceres::SubsetParameterization{2, {0}});
        //     break;
        //   case 1:
        //     problem.SetParameterization(topo.xy.col(ixy).data(),
        //                                 new ceres::SubsetParameterization{2, {1}});
        //     break;
        //   case 2:
        //     problem.SetParameterBlockConstant(topo.xy.col(ixy).data());
        //     break;
        // }
      }

      // for (long int i = 0; i < topo.tiles.cols(); i++) {
      //   for (long int j = 0; j < topo.tiles.rows(); j++) {
      //     auto i1{topo.tiles(j, i)};
      //     auto i2{topo.tiles((j + 1) % topo.tiles.rows(), i)};
      //     auto i3{topo.tiles((j + 2) % topo.tiles.rows(), i)};
      //     problem.AddResidualBlock(
      //         new ceres::AutoDiffCostFunction<ConvexResidual, 1, 2, 2, 2>(new ConvexResidual()),
      //         NULL, topo.xy.col(i1).data(), topo.xy.col(i2).data(), topo.xy.col(i3).data());
      //   }
      // }

      // todo: only min/max?
      for (long i = 0; i < topo.xy.cols(); i++) {
        problem.SetParameterLowerBound(topo.xy.col(i).data(), 0, 0);
        problem.SetParameterLowerBound(topo.xy.col(i).data(), 1, 0);
        problem.SetParameterUpperBound(topo.xy.col(i).data(), 0, frame.cols);
        problem.SetParameterUpperBound(topo.xy.col(i).data(), 1, frame.rows);
      }

      for (int i = 0; i < (int)colorx.cols(); i++) {
        for (int j = 0; j < 3; j++) {
          problem.SetParameterLowerBound(&colorx(0, i), j, 0);
          problem.SetParameterUpperBound(&colorx(0, i), j, 255);
        }
      }
      // Setting options
      options.num_threads = 8;  // todo
      options.minimizer_progress_to_stdout = true;
      options.max_num_iterations = 2000;
    }
    auto Solve() { ceres::Solve(options, &problem, &summary); }
    ceres::Problem problem;
    ceres::Solver::Options options;
    ceres::Solver::Summary summary;
    Topo topo;
    Eigen::Matrix3Xd colorx;  // todo: pack params?
  };

  template <typename T>
  auto BuildImage(const cv::Mat& frame, const Topo& topo, const Eigen::MatrixBase<T>& colorx) {
    auto output{cv::Mat{frame.size(), CV_8UC3, cv::Vec3b{0, 255, 0}}};
    auto xy{topo.xy};
    xy.row(0) *= frame.cols / topo.xy(0, Eigen::last);
    xy.row(1) *= frame.rows / topo.xy(1, Eigen::last);
    for (long int i = 0; i < topo.tiles.cols(); i++) {
      auto col{cv::Vec3b{(uchar)colorx(0, i), (uchar)colorx(1, i), (uchar)colorx(2, i)}};
      auto pts{std::vector<std::vector<cv::Point2i>>{1}};
      for (long int j = 0; j < topo.tiles.rows(); j++) {
        auto pteig{xy.col(topo.tiles(j, i))};
        auto ptcv{cv::Point2i{(int)pteig(0), (int)pteig(1)}};
        pts[0].push_back(ptcv);
      }

      cv::fillPoly(output, pts, col);
    }
    for (long i = 0; i < topo.tiles.cols(); i++) {
      for (size_t j = 0; j < 4; j++) {
        auto x0 = (int)topo.xy(0, topo.tiles(j, i));
        auto y0 = (int)topo.xy(1, topo.tiles(j, i));
        auto x1 = (int)topo.xy(0, topo.tiles((j + 1) % 4, i));
        auto y1 = (int)topo.xy(1, topo.tiles((j + 1) % 4, i));
        cv::line(output, {x0, y0}, {x1, y1}, {0, 0, 0});
      }
    }

    return output;
  }

}  // namespace optimos

//     for (long int i = 0; i < topo.tiles.cols(); i++) {
//       for (long int j = 0; j < topo.tiles.rows(); j++) {
//         auto i1{topo.tiles(j, i)};
//         auto i2{topo.tiles((j + 1) % topo.tiles.rows(), i)};
//         auto i3{topo.tiles((j + 2) % topo.tiles.rows(), i)};
//         auto convres{new ConvexResidual()};
//         auto autodiff{new ceres::AutoDiffCostFunction<ConvexResidual, 1, 2, 2,
//         2>(convres)}; problem.AddResidualBlock(autodiff, NULL, &topo.xy(0, i1), &topo.xy(0,
//         i2),
//                                  &topo.xy(0, i3));
//       }
//     }
