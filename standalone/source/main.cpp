#include <optimos/optimos.h>
#include <optimos/version.h>

#include <cxxopts.hpp>
#include <iostream>
#include <string>
#include <unordered_map>

using namespace optimos;

int main(int argc, char** argv) {
  cxxopts::Options options("Optimos", "Optimal rectangle mosaic of image!");
  // clang-format off
  options.add_options()
    ("h,help", "Show help")
    ("v,version", "Print the current version number")
    ("i,input", "Path to image input",  cxxopts::value<std::string>())
    ("o,output", "Path to image output", cxxopts::value<std::string>())
    ("r,row","Number of tile rows",cxxopts::value<int>())
    ("c,col","Number of tile columns",cxxopts::value<int>())
  ;
  // clang-format on

  auto result = options.parse(argc, argv);

  if (result.count("help")) {
    std::cout << options.help() << std::endl;
    return 0;
  } else if (result.count("version")) {
    std::cout << "Optimos, version " << OPTIMOS_VERSION << std::endl;
    return 0;
  }

  if (!result.count("input")) {
    std::cerr << "Input image missing! (-i <image_path>)" << std::endl;
    return -1;
  }
  auto im_i{result["input"].as<std::string>()};
  if (!result.count("output")) {
    std::cerr << "Ouput path missing! (-o <output_path>)" << std::endl;
    return -1;
  }
  auto im_o{result["output"].as<std::string>()};
  if (!result.count("row")) {
    std::cerr << "Number of rows missing! (-r <n_rows>)" << std::endl;
    return -1;
  }
  auto r{result["row"].as<int>()};
  if (!result.count("col")) {
    std::cerr << "Number of columns missing! (-c <n_cols>)" << std::endl;
    return -1;
  }
  auto c{result["col"].as<int>()};

  auto frame{cv::imread(im_i)};
  auto mospb{MosaicProblem(frame, r, c)};
  mospb.Solve();
  std::cout << mospb.summary.FullReport() << std::endl;
  auto resi{BuildImage(frame, mospb.topo, mospb.colorx)};
  cv::imwrite(im_o, resi);

  return 0;
}
