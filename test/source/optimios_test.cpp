#define DOCTEST_CONFIG_NO_SHORT_MACRO_NAMES
#include <doctest/doctest.h>
#include <optimos/optimos.h>
#include <optimos/version.h>

#include <iostream>
#include <opencv2/highgui.hpp>
#include <string>

#include "ceres/ceres.h"
#include "ceres/cubic_interpolation.h"

using namespace optimos;

DOCTEST_TEST_CASE("MakeXY") {
  const auto r = 2;
  const auto c = 3;
  auto [xy, tiles, constidx] = MakeTopoRect(r, c, 200, 300);
  DOCTEST_SUBCASE("XY mesh point coordinates") {
    auto expected{Eigen::Matrix2Xd(2, 3 * 4)};
    expected.row(0) << 0, 0, 0, 100, 100, 100, 200, 200, 200, 300, 300, 300;
    expected.row(1) << 0, 100, 200, 0, 100, 200, 0, 100, 200, 0, 100, 200;
    DOCTEST_CHECK_EQ(expected, xy);
  }
  DOCTEST_SUBCASE("tile points") {
    auto expected{Eigen::MatrixXi(4, 6)};
    // clang-format off
    expected << 0,3, 6,1,4, 7,
                3,6, 9,4,7,10,
                4,7,10,5,8,11,
                1,4,7 ,2,5, 8;
    // clang-format on
    DOCTEST_CHECK_EQ(expected, tiles);
  }
  DOCTEST_SUBCASE("constant edges") {
    auto expected{Eigen::Matrix2Xi(2, 10)};
    // clang-format off
    expected << 0, 3, 6, 9, 10, 11, 8, 5, 2, 1,
                2, 1, 1, 2,  0,  2, 1, 1, 2, 0;
    // clang-format on
    DOCTEST_CHECK_EQ(expected, constidx);
  }
}

DOCTEST_TEST_CASE("Polygon area") {
  DOCTEST_SUBCASE("Double case") {
    auto poly{Eigen::Matrix2Xd(2, 4)};
    auto p0{Eigen::Vector2d{1., 2.}};
    auto p1{Eigen::Vector2d{3., 2.}};
    auto p2{Eigen::Vector2d{3., 5.}};
    auto p3{Eigen::Vector2d{1., 5.}};
    auto output{PolyArea(p0, p1, p2, p3)};
    DOCTEST_CHECK_EQ(output, 6);
    auto a{456.456};
    auto R{Eigen::Matrix2d{}};
    R << cos(a), -sin(a), sin(a), cos(a);
    DOCTEST_CHECK_EQ(PolyArea(R * p0, R * p1, R * p2, R * p3), 6);
  }
  // todo: works but to rewirte with new api
  // DOCTEST_SUBCASE("Jet case") {
  //   using J = ceres::Jet<double, 8>;
  //   auto poly{Eigen::Matrix<J, 2, Eigen::Dynamic>(2, 4)};
  //   // clang-format off
  //   poly << J(0,0), J(2,2), J(2,4), J(0,6),
  //           J(0,1), J(0,3), J(3,5), J(3,7);
  //   // clang-format on
  //   auto output{PolyArea(poly)};
  //   auto expected{J(6)};
  //   expected.v << -1.5, -1, 1.5, -1, 1.5, 1, -1.5, 1;
  //   DOCTEST_CHECK_EQ(expected.a, output.a);
  //   DOCTEST_CHECK_EQ(expected.v, output.v);
  // }
}

DOCTEST_TEST_CASE("Point in quadrilatere") {
  DOCTEST_SUBCASE("Vertice check") {
    auto p0{Eigen::Vector2d{1.2, 5.1}};
    auto p1{Eigen::Vector2d{1.2, 5.1}};
    auto p2{Eigen::Vector2d{1.2, 5.1}};
    auto p3{Eigen::Vector2d{1.2, 5.1}};
    DOCTEST_CHECK_EQ(PointInQuadri(p0, p1, p2, p3, 0, 0), p0);
    DOCTEST_CHECK_EQ(PointInQuadri(p0, p1, p2, p3, 1, 0), p1);
    DOCTEST_CHECK_EQ(PointInQuadri(p0, p1, p2, p3, 1, 1), p2);
    DOCTEST_CHECK_EQ(PointInQuadri(p0, p1, p2, p3, 0, 1), p3);
  }
}

DOCTEST_TEST_CASE("Make and solve") {
  //   // google::InitGoogleLogging("Make and solve");
  DOCTEST_SUBCASE("Quadrilatere in image: 3x3 solving") {
    auto frame{cv::Mat(1080, 1920, CV_8UC3, {11, 44, 88})};
    auto poly{std::vector<std::vector<cv::Point>>{1}};
    auto p0{cv::Point{500, 300}};
    auto p1{cv::Point{1300, 200}};
    auto p2{cv::Point{1200, 700}};
    auto p3{cv::Point{600, 600}};
    poly[0] = {p0, p1, p2, p3};
    cv::fillPoly(frame, poly, {140, 110, 100});
    cv::imwrite("test.png", frame);
    auto mospb{MosaicProblem(frame, 3, 3)};
    mospb.Solve();
    std::cout << mospb.summary.FullReport() << std::endl;
    auto resi{BuildImage(frame, mospb.topo, mospb.colorx)};
    cv::imwrite("result.png", resi);
    // position check
    std::cout << (mospb.topo.xy.col(5) - Eigen::Vector2d{p0.x, p0.y}) << std::endl;
    DOCTEST_CHECK_LT((mospb.topo.xy.col(5) - Eigen::Vector2d{p0.x, p0.y}).norm(), 5);
    DOCTEST_CHECK_LT((mospb.topo.xy.col(6) - Eigen::Vector2d{p3.x, p3.y}).norm(), 5);
    DOCTEST_CHECK_LT((mospb.topo.xy.col(9) - Eigen::Vector2d{p1.x, p1.y}).norm(), 5);
    DOCTEST_CHECK_LT((mospb.topo.xy.col(10) - Eigen::Vector2d{p2.x, p2.y}).norm(), 5);
    // color check
    DOCTEST_CHECK_LT((mospb.colorx.col(0) - Eigen::Vector3d{11, 44, 88}).norm(), 10);
    DOCTEST_CHECK_LT((mospb.colorx.col(4) - Eigen::Vector3d{140, 110, 100}).norm(), 10);
  }

  //   DOCTEST_SUBCASE("Simple case: row image, constant color") {
  //     auto frame{cv::Mat{cv::Size{3, 1}, CV_8UC3}};
  //     {  // frame construction
  //       frame({0, 0, 2, 1}) = cv::Vec3b{0, 0, 0};
  //       frame({2, 0, 1, 1}) = cv::Vec3b{100, 100, 100};
  //     }
  //     std::cout << frame << std::endl;
  //     auto mospb{MosaicProblem<1, 2>(frame)};
  //     {  // problem definition and constraints
  //       mospb.colorx.col(0) << 0, 0, 0;
  //       mospb.colorx.col(1) << 100, 100, 100;
  //       mospb.problem.SetParameterBlockConstant(&mospb.colorx(0, 0));
  //       mospb.problem.SetParameterBlockConstant(&mospb.colorx(0, 1));
  //     }

  //     DOCTEST_CHECK_EQ(mospb.problem.NumParameters(), 2 * 3 * 2 + 2 * 3);
  //     auto pb{std::vector<double*>{}};
  //     mospb.problem.GetParameterBlocks(&pb);
  //     {  // param block size checks
  //       for (size_t i = 0; i < 6; i++) {
  //         DOCTEST_CHECK_EQ(mospb.problem.ParameterBlockSize(pb[i]), 2);
  //       }
  //       for (size_t i = 0; i < 2; i++) {
  //         DOCTEST_CHECK_EQ(mospb.problem.ParameterBlockSize(pb[6 + i]), 3);
  //       }
  //     }
  //     // mospb.problem.SetParameterization(pb[0], new ceres::SubsetParameterization{2, {0, 1}});
  //     {  // param blocks emplacements
  //       for (size_t i = 0; i < 6; i++) {
  //         DOCTEST_CHECK_EQ(pb[i], &mospb.topo.xy(0, i));
  //       }
  //       DOCTEST_CHECK_EQ(pb[6], &mospb.colorx(0, 0));
  //       DOCTEST_CHECK_EQ(pb[7], &mospb.colorx(0, 1));
  //       for (auto&& i : {0, 1, 4, 5, 6, 7}) {
  //         // DOCTEST_CHECK_EQ(mospb.problem.ParameterBlockLocalSize(pb[i]), 0);
  //         DOCTEST_CHECK_EQ(mospb.problem.IsParameterBlockConstant(pb[i]), true);
  //       }
  //       for (auto&& i : {2, 3}) {
  //         DOCTEST_CHECK_EQ(mospb.problem.ParameterBlockLocalSize(pb[i]), 1);
  //         DOCTEST_CHECK_EQ(mospb.problem.IsParameterBlockConstant(pb[i]), false);
  //       }
  //     }
  //     auto lp = mospb.problem.GetParameterization(pb[2]);
  //     double x[]{1, 2};
  //     double delta[]{100, 100};
  //     double x_plus_delta[]{0, 0};
  //     lp->Plus(x, delta, x_plus_delta);
  //     double expected[]{101, 2};
  //     for (size_t i = 0; i < 2; i++) {
  //       DOCTEST_CHECK_EQ(x_plus_delta[i], expected[i]);
  //     }
  //     mospb.Solve();
  //     std::cout << mospb.summary.FullReport() << std::endl;
  //     std::cout << mospb.topo.xy << std::endl;
  //     auto residual_blocks{std::vector<ceres::ResidualBlockId>{}};
  //     mospb.problem.GetResidualBlocks(&residual_blocks);
  //     DOCTEST_CHECK_LT(abs(mospb.topo.xy(0, 3) - 2), 1e-6);
  //   }
  //   DOCTEST_SUBCASE("Simple case: mid node convergence") {
  //     auto frame{cv::Mat{cv::Size{6, 4}, CV_8UC3}};
  //     frame({0, 0, 4, 3}) = cv::Vec3b{1, 1, 1};
  //     frame({4, 3, 2, 1}) = cv::Vec3b{200, 200, 200};
  //     frame({4, 0, 2, 3}) = cv::Vec3b{44, 44, 44};
  //     frame({0, 3, 4, 1}) = cv::Vec3b{5, 5, 5};
  //     auto mospb{MosaicProblem<2, 2>(frame)};
  //     mospb.Solve();
  //     auto intersect{Eigen::Vector2d{}};
  //     intersect << 4, 3;
  //     std::cout << mospb.topo.xy << std::endl;
  //     DOCTEST_CHECK_LT((mospb.topo.xy.col(4) - intersect).norm(), 1e-6);
  //     DOCTEST_CHECK_LT(mospb.summary.final_cost, 1e-6);
  //   }

  //   DOCTEST_SUBCASE("real life example to move in standalone") {
  //     auto frame{cv::imread("test/datatest/cat.jpg")};
  //     auto small{cv::Mat{}};
  //     const auto r{50};
  //     const auto c{50};
  //     cv::resize(frame, small, {c, r});
  //     cv::imwrite("small.png", small);
  //     auto mospb{MosaicProblem<7, 7>(small)};
  //     mospb.Solve();
  //     auto outf{BuildImage(frame, mospb.topo, mospb.colorx)};
  //     cv::imwrite("output.png", outf);
  //     std::cout << "XXXX" << std::endl;

  //     // cv::imshow("output", outf);
  //     // cv::waitKey(0);
  //   }
}

// DOCTEST_TEST_CASE("Optimos version") {
//   static_assert(std::string_view(OPTIMOS_VERSION) == std::string_view("1.0"));
//   DOCTEST_CHECK(std::string(OPTIMOS_VERSION) == std::string("1.0"));
// }

// todo: r,c not needed as template params anymore i think?

// DOCTEST_TEST_CASE("is inside") {
//   Eigen::Vector2d p{0, 1};
//   using J = ceres::Jet<double, 1>;
//   Eigen::Matrix<J, 2, 1> e0;
//   e0 << J(2.), J(-1.);
//   Eigen::Matrix<J, 2, 1> e1;
//   e1 << J(2.), J(3.);
//   DOCTEST_CHECK_EQ(IsInside(p, e0, e1), true);
//   DOCTEST_CHECK_EQ(IsInside(p, e1, e0), false);
// }

// DOCTEST_TEST_CASE("intersect") {
//   DOCTEST_SUBCASE("normal intersection") {
//     Eigen::Vector2d e0{0, 1};
//     Eigen::Vector2d e1{4, 1};
//     using J = ceres::Jet<double, 2>;
//     Eigen::Matrix<J, 2, 1> p0;
//     p0 << J(2.), J(-1.);
//     Eigen::Matrix<J, 2, 1> p1;
//     p1 << J(2., 0), J(3.);
//     auto output{Intersection(p0, p1, e0, e1)};
//     auto expected{Eigen::Matrix<J, 2, 1>()};
//     expected << J(2), J(1);
//     expected(0).v(0) = 2. / 4;

//     DOCTEST_CHECK_EQ(output(0).a, expected(0).a);
//     DOCTEST_CHECK_EQ(output(1).a, expected(1).a);
//     DOCTEST_CHECK_EQ(output(0).v, expected(0).v);
//     DOCTEST_CHECK_EQ(output(1).v, expected(1).v);
//   }
//   // DOCTEST_SUBCASE("Colinear intersection") {
//   //   Eigen::Vector2d e0{0, 1};
//   //   Eigen::Vector2d e1{4, 1};
//   //   using J = ceres::Jet<double, 2>;
//   //   Eigen::Matrix<J, 2, 1> p0;
//   //   p0 << J(0.), J(1.);
//   //   Eigen::Matrix<J, 2, 1> p1;
//   //   p1 << J(4., 0), J(1.);
//   //   auto output{Intersection(p0, p1, e0, e1)};
//   //   auto expected{Eigen::Matrix<J, 2, 1>()};
//   //   expected(0).v(0) = std::nan();

//   //   DOCTEST_CHECK_EQ(output(0).a, expected(0).a);
//   //   DOCTEST_CHECK_EQ(output(1).a, expected(1).a);
//   //   DOCTEST_CHECK_EQ(output(0).v, expected(0).v);
//   //   DOCTEST_CHECK_EQ(output(1).v, expected(1).v);
//   // }
// }

// DOCTEST_TEST_CASE("Polygon convex clipping") {
//   {
//     auto poly{Eigen::Matrix2Xd(2, 4)};
//     // clang-format off
//   poly << 2, 4,   4,   2,
//           3,   3,   7, 7;

//     // clang-format on
//     auto clipper{Eigen::Matrix2Xd(2, 4)};
//     // clang-format off
//   clipper <<0,10,10, 0,
//           0, 0, 10, 10;
//     // clang-format on
//     auto idx{Eigen::VectorXi{4}};
//     idx << 0, 1, 2, 3;
//     DOCTEST_SUBCASE("clip outside: no clipping") {
//       auto output{Clip(poly, idx, clipper)};
//       auto expected{poly};
//       DOCTEST_CHECK_EQ(output, expected);
//     }
//     DOCTEST_SUBCASE("clip inside: full clipping") {
//       std::swap(clipper, poly);
//       auto output{Clip(poly, idx, clipper)};
//       auto expected{clipper};
//       DOCTEST_CHECK_EQ(output, expected);
//     }
//   }
//   DOCTEST_SUBCASE("Polygon Jet Clipping") {
//     using J = ceres::Jet<double, 1>;
//     auto polyJ{Eigen::Matrix<J, 2, 4>{}};
//     // clang-format off
//     polyJ << J(0.5), J(1.5), J(1.5), J(0.5,0),
//              J(0.5), J(0.5), J(1.5), J(1.5);
//     // clang-format on
//     auto idxJ{Eigen::Vector4i{0, 1, 2, 3}};
//     auto clipperJ{Eigen::Matrix2Xd{2, 4}};
//     // clang-format off
//     clipperJ <<  0, 1, 1, 0,
//                 0, 0, 1, 1;
//     // clang-format on
//     auto output{Clip(polyJ, idxJ, clipperJ)};
//     DOCTEST_CHECK_EQ(output.col(3)(0).v(0), 0.5);
//   }
//   DOCTEST_SUBCASE("polygon clipping with common edge") {
//     auto poly{Eigen::Matrix2Xd(2, 4)};
//     // clang-format off
//     poly << 0,   2,   2,  0,
//             0,   0,   2,  2;

//     // clang-format on
//     auto clipper{Eigen::Matrix2Xd(2, 4)};
//     // clang-format off
//     clipper <<0, 1, 1, 0,
//               0, 0, 1, 1;
//     // clang-format on
//     auto idx{Eigen::VectorXi{4}};
//     idx << 0, 1, 2, 3;
//     auto output{Clip(poly, idx, clipper)};
//     auto expected{Eigen::Matrix2Xd(2, 4)};
//     // clang-format off
//     expected << 0,   1,   1,  0,
//             0,   0,   1,  1;

//     // clang-format on
//     DOCTEST_CHECK_EQ(expected, output);
//   }
//   DOCTEST_SUBCASE("if Clipper=poly, jets should remain") {
//     auto clip{Eigen::Matrix2Xd(2, 4)};
//     // clang-format off
//     clip << 0,   2,   2,  0,
//             0,   0,   3,  3;

//     // clang-format on
//     using J = ceres::Jet<double, 8>;
//     auto poly{Eigen::Matrix<J, 2, Eigen::Dynamic>(2, 4)};
//     // clang-format off
//     poly << J(0,0), J(2,2), J(2,4), J(0,6),
//             J(0,1), J(0,3), J(3,5), J(3,7);
//     // clang-format on
//     auto idx{Eigen::VectorXi{4}};
//     idx << 0, 1, 2, 3;
//     auto output{Clip(poly, idx, clip)};
//     auto expected{poly};
//     // todo: explicit check of jets cause not checked
//     DOCTEST_CHECK_EQ(expected, output);
//   }
// }

// DOCTEST_TEST_CASE("Color Residual") {
//   auto topo{MakeTopoRect<1, 1>(1, 1)};
//   // auto& xy{topo.xy};
//   auto& tiles{topo.tiles};
//   std::cout << tiles << std::endl;
//   // auto& cidx{topo.constidx};
//   auto xy_pix{Eigen::Matrix<double, 2, 4>()};
//   // clang-format off
//   xy_pix << 0, 1, 1, 0,
//             0, 0, 1, 1;
//   // clang-format on
//   int nxy = 4;
//   auto colorpix{cv::Vec3b{1, 2, 3}};
//   auto color_res{ColorResidual{colorpix, xy_pix, tiles, nxy}};
//   Eigen::Vector3d col;
//   col << 1, 2, 3;
//   Eigen::Vector3d residual;
//   auto paramblocks{MakeParameterBlocks(topo, col)};
//   color_res(paramblocks.data(), residual.data());
//   DOCTEST_CHECK_EQ(residual, Eigen::Vector3d::Zero());
//   //
//   col << 0, 0, 0;
//   color_res(&paramblocks[0], residual.data());
//   DOCTEST_CHECK_EQ(residual, Eigen::Vector3d::LinSpaced(3, 1, 3));
// }